﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBugTracker.Models
{
    public class Attachment
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        [Required]
        public string Body { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset Created { get; set; }
        public string AuthorUserId { get; set; }
        public string FileUrl { get; set; }
        public string FileDisplayName { get; set; }

        public virtual KingBugTracker.Models.ApplicationUser AuthorUser { get; set; }

        public virtual KingBugTracker.Models.Ticket Ticket { get; set; }
    }
}