﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBugTracker.Models
{
    public class Project
    {
        public Project()
        {
            this.Tickets = new HashSet<KingBugTracker.Models.Ticket>();
            this.Users = new HashSet<KingBugTracker.Models.ApplicationUser>();
        }

        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        public bool IsDeleted { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset Created { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset? Updated { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}