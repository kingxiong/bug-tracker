﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBugTracker.Models
{
    public class History
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        [Required]
        public string Property { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset ChangeDate { get; set; }
        public string UserId { get; set; }

        public virtual KingBugTracker.Models.ApplicationUser AuthorUser { get; set; }
        public virtual KingBugTracker.Models.Ticket Ticket { get; set; }
    }
}