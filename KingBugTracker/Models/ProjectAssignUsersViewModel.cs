﻿using System.Web.Mvc;

namespace KingBugTracker.Models
{
    public class ProjectAssignUsersViewModel
    {
        public Project Project { get; set; }

        public MultiSelectList Users { get; set; }
        public string[] SelectedUsers { get; set; }
    }
}