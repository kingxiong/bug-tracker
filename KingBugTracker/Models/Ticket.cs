﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KingBugTracker.Models
{
    public class Ticket
    {
        public Ticket()
        {
            this.Comments = new HashSet<Comment>();
            this.Attachments = new HashSet<Attachment>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        public bool IsDeleted { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset Created { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset? Updated { get; set; }
        public int ProjectId { get; set; }
        public int TypeId { get; set; }
        public int PriorityId { get; set; }
        public int StatusId { get; set; }

        public string AuthorUserId { get; set; }
        public string AssignedToUserId { get; set; }

        public virtual KingBugTracker.Models.Type Type { get; set; }
        public virtual KingBugTracker.Models.Priority Priority { get; set; }
        public virtual KingBugTracker.Models.Status Status { get; set; }

        public virtual KingBugTracker.Models.ApplicationUser AuthorUser { get; set; }
        public virtual KingBugTracker.Models.ApplicationUser AssignedToUser { get; set; }
        public virtual KingBugTracker.Models.Project Project { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<History> Histories { get; set; }
    }
}