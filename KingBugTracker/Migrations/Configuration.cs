namespace KingBugTracker.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using KingBugTracker.Models;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KingBugTracker.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(KingBugTracker.Models.ApplicationDbContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(
                new RoleStore<IdentityRole>(context));

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }

            if (!context.Roles.Any(r => r.Name == "Project Manager"))
            {
                roleManager.Create(new IdentityRole { Name = "Project Manager" });
            }

            if (!context.Roles.Any(r => r.Name == "Developer"))
            {
                roleManager.Create(new IdentityRole { Name = "Developer" });
            }

            if (!context.Roles.Any(r => r.Name == "Submitter"))
            {
                roleManager.Create(new IdentityRole { Name = "Submitter" });
            }

            if (!context.Roles.Any(r => r.Name == "Guest"))
            {
                roleManager.Create(new IdentityRole { Name = "Guest" });
            }

            var userManager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));

            if (!context.Users.Any(u => u.Email == "kxiong388@gmail.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "King",
                    LastName = "Xiong",
                    PhoneNumber = "(828) 461-6238",
                    DisplayName = "Kingaroo",
                    UserName = "kxiong388@gmail.com",
                    Email = "kxiong388@gmail.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "sszpunar@coderfoundry.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Sean",
                    LastName = "Szpunar",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Spooner",
                    UserName = "sszpunar@coderfoundry.com",
                    Email = "sszpunar@coderfoundry.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "ewatkins@coderfoundry.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Eric",
                    LastName = "Watkins",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Radio Voice",
                    UserName = "ewatkins@coderfoundry.com",
                    Email = "ewatkins@coderfoundry.com",
                }, "Asdf123!");
            }
            if (!context.Users.Any(u => u.Email == "barrychau93@gmail.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Barry",
                    LastName = "Chau",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Burrito",
                    UserName = "barrychau93@gmail.com",
                    Email = "barrychau93@gmail.com",
                }, "Asdf123!");
            }

            if (!context.Users.Any(u => u.Email == "admin@bugtracker.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Admin",
                    LastName = "Adam",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Adam",
                    UserName = "admin@bugtracker.com",
                    Email = "admin@bugtracker.com",
                }, "Asdf123!");
            }

            if (!context.Users.Any(u => u.Email == "project.manager@bugtracker.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Project",
                    LastName = "Manager",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "PM",
                    UserName = "projectmanager@bugtracker.com",
                    Email = "projectmanager@bugtracker.com",
                }, "Asdf123!");
            }

            if (!context.Users.Any(u => u.Email == "developer@bugtracker.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Developer",
                    LastName = "Dev",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Dev",
                    UserName = "developer@bugtracker.com",
                    Email = "developer@bugtracker.com",
                }, "Asdf123!");
            }

            if (!context.Users.Any(u => u.Email == "submitter@bugtracker.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Submitter",
                    LastName = "Sub",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Sub",
                    UserName = "submitter@bugtracker.com",
                    Email = "submitter@bugtracker.com",
                }, "Asdf123!");
            }

            if (!context.Users.Any(u => u.Email == "guest@bugtracker.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    FirstName = "Guest",
                    LastName = "Guest",
                    PhoneNumber = "(###) ###-####",
                    DisplayName = "Guest",
                    UserName = "guest@bugtracker.com",
                    Email = "guest@bugtracker.com",
                }, "Asdf123!");
            }
            //Admins
            var userId_King = userManager.FindByEmail("kxiong388@gmail.com").Id;
            userManager.AddToRole(userId_King, "Admin");
            var userId_Sean = userManager.FindByEmail("sszpunar@coderfoundry.com").Id;
            userManager.AddToRole(userId_Sean, "Admin");
            var userId_Eric = userManager.FindByEmail("ewatkins@coderfoundry.com").Id;
            userManager.AddToRole(userId_Eric, "Admin");
            var userId_Admin = userManager.FindByEmail("admin@bugtracker.com").Id;
            userManager.AddToRole(userId_Admin, "Admin");

            //PM
            var userId_Barry = userManager.FindByEmail("barrychau93@gmail.com").Id;
            userManager.AddToRole(userId_Barry, "Project Manager");
            
            var userId_ProjectManager = userManager.FindByEmail("projectmanager@bugtracker.com").Id;
            userManager.AddToRole(userId_ProjectManager, "Project Manager");

            //Developer
            var userId_Developer = userManager.FindByEmail("developer@bugtracker.com").Id;
            userManager.AddToRole(userId_Developer, "Developer");

            //Submitter
            var userId_Submitter = userManager.FindByEmail("submitter@bugtracker.com").Id;
            userManager.AddToRole(userId_Submitter, "Submitter");

            //Guest
            var userId_Guest = userManager.FindByEmail("guest@bugtracker.com").Id;
            userManager.AddToRole(userId_Guest, "Admin");

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}