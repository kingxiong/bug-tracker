﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBugTracker.Models;
using Microsoft.AspNet.Identity;

namespace KingBugTracker.Controllers
{
    public class CommentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Comments
        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.AuthorUser).Include(c => c.Ticket);
            return View(comments.ToList());
        }

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Body,Created,Updated,TicketId,AuthorUserId")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                comment.AuthorUserId = User.Identity.GetUserId();
                comment.Created = new DateTimeOffset(DateTime.Now);
                Ticket ticket = new Ticket();
                //Ticket oldTicket = new ApplicationDbContext().Tickets.Find(ticket.Id);
                ticket = db.Tickets.Find(comment.TicketId);
                //var history = new History
                //{
                //    ChangeDate = new DateTimeOffset(DateTime.Now),
                //    TicketId = ticket.Id,
                //    UserId = User.Identity.GetUserId(),
                //    Property = "Type",
                //    OldValue = oldTicket.Type.Name,
                //    NewValue = db.Types.Find(ticket.TypeId).Name
                //};
                //db.Histories.Add(history);
                if (ticket.AssignedToUserId != null)
                {
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "Comments has been made for " + ticket.Title,
                        Type = "Comment",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                }

                db.Comments.Add(comment);
                
                db.SaveChanges();
                var thisPost = db.Tickets.Find(comment.TicketId);
                if (thisPost != null)
                {
                    return RedirectToAction("Details", "Tickets", new { id = thisPost.Id });// redirect to details page in Blog Post. 
                }
            }

            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comment.TicketId);
            return RedirectToAction("Index", "Tickets");
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comment.TicketId);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Body,Created,Updated,TicketId,AuthorUserId")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comment.TicketId);
            return View(comment);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
