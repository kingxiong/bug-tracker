﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBugTracker.Models;
using Microsoft.AspNet.Identity;
using KingBugTracker.Models.Helpers;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
namespace KingBugTracker.Controllers
{
    public class TicketsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tickets
        public ActionResult Index()
        {
            var tickets = db.Tickets.Include(t => t.AssignedToUser).Include(t => t.AuthorUser).Include(t => t.Priority).Include(t => t.Project).Include(t => t.Status).Include(t => t.Type);
            return View(tickets.ToList());
        }

        // GET: Tickets/TicketAssign
        [Authorize]
        public ActionResult TicketAssign(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ApplicationDbContext context = new ApplicationDbContext();
            Project project = db.Projects.Find(ticket.ProjectId);
            var projectUsers = context.Users.Where(u => u.Projects.Any(p => p.Title == project.Title));
            var role = context.Roles.SingleOrDefault(u => u.Name == "Developer");
            var usersInRole = context.Users.Where(u => u.Roles.Any(r => (r.RoleId == role.Id)));
            var displayUsers = usersInRole.Where(u => u.Projects.Any(p => (p.Title == project.Title)));
            var removeUser = db.Users.Where(u => (u.DisplayName != "N/A" && u.DisplayName != "(Remove Assigned User)"));
            ViewBag.AssignedToUserId = new SelectList(displayUsers, "Id", "DisplayName", ticket.AssignedToUserId);
            return View(ticket);
        }
        // POST: Tickets/TicketAssign
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TicketAssign([Bind(Include = "Id,Title,Body,Created,Updated,ProjectId,TypeId,PriorityId,StatusId,AuthorUserId,AssignedToUserId")] Ticket ticket, string oldAssignedToUserId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            Project project = db.Projects.Find(ticket.ProjectId);

            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                 if (ticket.AssignedToUserId != null)
                {
                    ticket.StatusId = 17;
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "You have been assigned to " + ticket.Title,
                        Type = "AssignedToUserId",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                }

                //15 = unassigned
                //17 = assigned
                
                ApplicationUser NotAssigned = db.Users.FirstOrDefault(u => u.UserName.Equals("unassigned@galileo.com"));
                ApplicationUser RemoveAssigned = db.Users.FirstOrDefault(u => u.UserName.Equals("removeassigned@galileo.com"));
                ticket.Updated = DateTimeOffset.Now;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            var projectUsers = context.Users.Where(u => u.Projects.Any(p => p.Title == project.Title));
            var role = context.Roles.SingleOrDefault(u => u.Name == "Developer");
            var usersInRole = context.Users.Where(u => u.Roles.Any(r => (r.RoleId == role.Id)));
            var displayUsers = usersInRole.Where(u => u.Projects.Any(p => (p.Title == project.Title)));
            var removeUser = db.Users.Where(u => (u.DisplayName != "N/A" && u.DisplayName != "(Remove Assigned User)"));
            ViewBag.AssignedToUserId = new SelectList(displayUsers, "Id", "DisplayName", ticket.AssignedToUserId);
            return View(ticket);
        }
        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
       
                return View(ticket);
        }

        // GET: Tickets/Create
        public ActionResult Create(int id)
        {

            ViewBag.AssignedToUserId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.PriorityId = new SelectList(db.Priorities, "ID", "Name");
            ViewBag.ProjectId = id;
            ViewBag.StatusId = new SelectList(db.Statuses, "Id", "Name");
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Body,Created,Updated,ProjectId,TypeId,PriorityId,StatusId,AuthorUserId,AssignedToUserId")] Ticket ticket, int pId)
        {
            if (ModelState.IsValid)
            {       
                ticket.AuthorUserId = User.Identity.GetUserId();
                ticket.ProjectId = pId;
                ticket.StatusId = 15;
                db.Tickets.Add(ticket);
                ticket.Created = new DateTimeOffset(DateTime.Now);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            ViewBag.AssignedToUserId = new SelectList(db.Users, "Id", "FirstName", ticket.AssignedToUserId);
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", ticket.AuthorUserId);
            ViewBag.PriorityId = new SelectList(db.Priorities, "ID", "Name", ticket.PriorityId);
            ViewBag.ProjectId = new SelectList(db.Projects, "Id", "Title", ticket.ProjectId);
            ViewBag.StatusId = new SelectList(db.Statuses, "Id", "Name", ticket.StatusId);
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name", ticket.TypeId);
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);

            if (ticket == null)
            {
                return HttpNotFound();
            }

            ViewBag.AssignedToUserId = new SelectList(db.Users, "Id", "FirstName", ticket.AssignedToUserId);
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", ticket.AuthorUserId);
            ViewBag.PriorityId = new SelectList(db.Priorities, "ID", "Name", ticket.PriorityId);
            ViewBag.ProjectId = new SelectList(db.Projects, "Id", "Title", ticket.ProjectId);
            ViewBag.StatusId = new SelectList(db.Statuses, "Id", "Name", ticket.StatusId);
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name", ticket.TypeId);
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Body,Updated,ProjectId,TypeId,PriorityId,StatusId,AuthorUserId,AssignedToUserId")] Ticket ticket)
        {
             if (ModelState.IsValid)
            {
                Ticket oldTicket = new ApplicationDbContext().Tickets.Find(ticket.Id);
                if (oldTicket.PriorityId != ticket.PriorityId)
                {
                    var history = new History
                    {
                        ChangeDate = new DateTimeOffset(DateTime.Now),
                        TicketId = ticket.Id,
                        UserId = User.Identity.GetUserId(),
                        Property = "Priority",
                        OldValue = oldTicket.Priority.Name,
                        NewValue = db.Priorities.Find(ticket.PriorityId).Name
                    };
                    db.Histories.Add(history);
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "The Priority for " + ticket.Title + "ticket has changed.",
                        Type = "Priority",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                    //db.SaveChanges();
                    //notify someone of these changes
                    await Notify(ticket.Id, history);
                }
                if (oldTicket.TypeId != ticket.TypeId)
                {
                    var history = new History
                    {
                        ChangeDate = new DateTimeOffset(DateTime.Now),
                        TicketId = ticket.Id,
                        UserId = User.Identity.GetUserId(),
                        Property = "Type",
                        OldValue = oldTicket.Type.Name,
                        NewValue = db.Types.Find(ticket.TypeId).Name
                    };
                    db.Histories.Add(history);
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "The Type for " + ticket.Title + "ticket has changed.",
                        Type = "Type",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                    //db.SaveChanges();
                    //notify someone of these changes
                    await Notify(ticket.Id, history);
                }
                if (oldTicket.StatusId != ticket.StatusId)
                {
                    var history = new History
                    {
                        ChangeDate = new DateTimeOffset(DateTime.Now),
                        TicketId = ticket.Id,
                        UserId = User.Identity.GetUserId(),
                        Property = "Status",
                        OldValue = oldTicket.Status.Name,
                        NewValue = db.Statuses.Find(ticket.StatusId).Name
                    };
                    db.Histories.Add(history);
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "The Status for " + ticket.Title + "ticket has changed.",
                        Type = "Status",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                    //db.SaveChanges();
                    //notify someone of these changes
                    await Notify(ticket.Id, history);
                }
                //AssignedToUser does not work because I don't have the form. 
                //if (oldTicket.AssignedToUserId != ticket.AssignedToUserId)
                //{
                //    var history = new History
                //    {
                //        ChangeDate = new DateTimeOffset(DateTime.Now),
                //        TicketId = ticket.Id,
                //        UserId = User.Identity.GetUserId(),
                //        Property = "Assigned User",
                //        OldValue = db.Users.Find(oldTicket.AssignedToUserId).DisplayName,
                //        NewValue = db.Users.Find(ticket.AssignedToUserId).DisplayName
                //    };
                //    db.Histories.Add(history);
                //    //db.SaveChanges();
                //    //notify someone of these changes
                //    await Notify(ticket.Id, history);
                //}
                if (oldTicket.Title != ticket.Title)
                {
                    var history = new History
                    {
                        ChangeDate = new DateTimeOffset(DateTime.Now),
                        TicketId = ticket.Id,
                        UserId = User.Identity.GetUserId(),
                        Property = "Title",
                        OldValue = oldTicket.Title,
                        NewValue = ticket.Title
                    };
                    db.Histories.Add(history);
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "The Title for " + ticket.Title + "ticket has changed.",
                        Type = "Title",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                    //db.SaveChanges();
                    //notify someone of these changes
                    await Notify(ticket.Id, history);
                }
                if (oldTicket.Body != ticket.Body)
                {
                    var history = new History
                    {
                        ChangeDate = new DateTimeOffset(DateTime.Now),
                        TicketId = ticket.Id,
                        UserId = User.Identity.GetUserId(),
                        Property = "Body",
                        OldValue = oldTicket.Body,
                        NewValue = ticket.Body
                    };
                    db.Histories.Add(history);
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "The Body for " + ticket.Title + "ticket has changed.",
                        Type = "Body",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                   
                    //db.SaveChanges();
                    //notify someone of these changes
                    await Notify(ticket.Id, history);
                }
                ticket.Updated = DateTimeOffset.Now;
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.AssignedToUserId = new SelectList(db.Users, "Id", "FirstName", ticket.AssignedToUserId);
            ViewBag.AuthorUserId = ticket.AuthorUserId;
            ViewBag.PriorityId = new SelectList(db.Priorities, "ID", "Name", ticket.PriorityId);
            ViewBag.ProjectId = new SelectList(db.Projects, "Id", "Title", ticket.ProjectId);
            ViewBag.StatusId = new SelectList(db.Statuses, "Id", "Name", ticket.StatusId);
            ViewBag.TypeId = new SelectList(db.Types, "Id", "Name", ticket.TypeId);
            return View(ticket);
        }
        
        public async Task Notify(int ticketId, History history)
        {
            var db = new ApplicationDbContext();
            var ticket = db.Tickets.Find(ticketId);
            //var project = db.Projects.Find(ticket.ProjectId);
            var displayName = db.Users.Find(history.UserId).DisplayName;
            var eService = new PersonalEmail();
            if (ticket.AssignedToUserId == User.Identity.GetUserId())
            {
                //user is assigned user, send notification to PM
                await eService.SendAsync(new MailMessage(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["emailTo"])
                {
                    Subject = "Ticket Change",
                    Body = ticket.AssignedToUser.UserName + ", <p/><b>" + displayName +
                           "</b> has changed the <b>'" + history.Property + "'</b> of ticket <b>'" +
                           ticket.Title + "'</b> from [old]<mark>" + history.OldValue + "</mark> to [new]<mark>" + history.NewValue + "</mark>.",
                    IsBodyHtml = true
                });
            }
            else
            {
                try
                {
                    //error: null exception reference
                    //description: an object is null and it shouldn't be
                    //user is PM or Admin, send notification to AssignedUser
                    await eService.SendAsync(new MailMessage(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["emailTo"])
                    {
                        Subject = "PM or Admin Ticket Change",
                        Body = User.Identity.GetUserName() + ", <p/><b>" + displayName +
                               "</b> has changed the <b>'" + history.Property + "'</b> of ticket <b>'" +
                               ticket.Title + "'</b> from [old]<mark>" + history.OldValue + "</mark> to [new]<mark>" + history.NewValue + "</mark>.",
                        IsBodyHtml = true
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await Task.FromResult(0);
                }
            }

        }
       

        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            ticket.IsDeleted = true;
            //db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
