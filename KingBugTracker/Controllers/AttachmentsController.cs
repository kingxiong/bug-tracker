﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBugTracker.Models;
using KingBugTracker.Models.Helpers;
using System.IO;
using Microsoft.AspNet.Identity;

namespace KingBugTracker.Controllers
{
    public class AttachmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Attachments
        public ActionResult Index()
        {
            var attachments = db.Attachments.Include(a => a.AuthorUser).Include(a => a.Ticket);
            return View(attachments.ToList());
        }

        // GET: Attachments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            return View(attachment);
        }

        // GET: Attachments/Create
        public ActionResult Create(int id)
        {
            ViewBag.TicketId = id;
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName");
            //ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title");
            return View();
        }

        // POST: Upload
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TicketId,Body,Created,AuthorUserId,FileUrl,FileDisplayName")] Attachment attachment, HttpPostedFileBase image, int id)
        {
            if (ModelState.IsValid)
            {
                if (image != null && image.ContentLength > 0)
                {
                    string userId = User.Identity.GetUserId();
                    attachment.AuthorUserId = userId;
                    attachment.Created = DateTimeOffset.Now;
                    attachment.TicketId = id;
                    var ext = Path.GetExtension(image.FileName).ToLower();
                    if (ext != ".png" && ext != ".jpg" && ext != ".jpeg" && ext != ".gif" && ext != ".gif" && ext != ".bmp" && ext != ".docx" && ext != ".pdf" && ext != ".txt")
                    {
                        ModelState.AddModelError("image", "Invalid Format.");
                    }
                    if (image != null)
                    {
                        var fileName = Path.GetFileName(image.FileName);
                        image.SaveAs(Path.Combine(Server.MapPath("~/File Uploads/"), fileName));
                        attachment.FileUrl = "/File Uploads/" + fileName;
                        attachment.FileDisplayName = fileName;
                    }
                }
                db.Attachments.Add(attachment);
                Ticket ticket = new Ticket();
                ticket = db.Tickets.Find(attachment.TicketId);
                //Ticket oldTicket = new ApplicationDbContext().Tickets.Find(ticket.Id);
                //var history = new History
                //{
                //    ChangeDate = new DateTimeOffset(DateTime.Now),
                //    TicketId = ticket.Id,
                //    UserId = User.Identity.GetUserId(),
                //    Property = "Type",
                //    OldValue = oldTicket.Type.Name,
                //    NewValue = db.Types.Find(ticket.TypeId).Name
                //};
                //db.Histories.Add(history);
                if (ticket.AssignedToUserId != null)
                {
                    var notify = new Notification
                    {
                        TicketId = ticket.Id,
                        Message = "An Attachment has been made for " + ticket.Title,
                        Type = "Attachment",
                        ChangeDate = DateTimeOffset.Now,
                        NotifyUserId = ticket.AssignedToUserId
                    };
                    db.Notifications.Add(notify);
                }

                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Details", "Tickets", new { id = attachment.TicketId});

            }
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", attachment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", attachment.TicketId);
            //return View(attachment);
            return RedirectToAction("Details", "Tickets", new { id = attachment.TicketId });


        }
        // GET: Attachments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", attachment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", attachment.TicketId);
            return View(attachment);
        }

        // POST: Attachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TicketId,Body,Created,AuthorUserId,FileUrl,FileDisplayName")] Attachment attachment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthorUserId = new SelectList(db.Users, "Id", "FirstName", attachment.AuthorUserId);
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", attachment.TicketId);
            return View(attachment);
        }

        // GET: Attachments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            return View(attachment);
        }

        // POST: Attachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Attachment attachment = db.Attachments.Find(id);
            db.Attachments.Remove(attachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
