﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KingBugTracker.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using PagedList.Mvc;

namespace KingBugTracker.Controllers
{
    public class ProjectsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ProjectAssignHelper projectHelper;
        private UserRolesHelper roleHelper;

        public ProjectsController()
        {
            projectHelper = new ProjectAssignHelper(db);
            roleHelper = new UserRolesHelper(db);
        }
        // GET: Projects
        public ActionResult Index()
        {

            return View();
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }


        // GET: Projects/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Body,Created,Updated,Users")] Project project)
        {
            if (ModelState.IsValid)
            {
                project.Created = new DateTimeOffset(DateTime.Now);

                //project.Users.Add(db.Users.First());
                db.Projects.Add(project);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(project);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Body,Created,Updated")] Project project)
        {
            if (ModelState.IsValid)
            {
                project.Updated = new DateTimeOffset(DateTime.Now);
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(project);
        }
        
        //Get
        [Authorize(Roles = "Admin, Project Manager, Guest")]
        public ActionResult ProjectAssign(int id)
        {
            var project = db.Projects.Find(id);
            ProjectAssignHelper helper = new ProjectAssignHelper(db);
            var model = new ProjectAssignUsersViewModel();

            model.Project = project;
            model.SelectedUsers = helper.ListAssignedUsers(id).Select(u => u.Id).ToArray();
            model.Users = new MultiSelectList(db.Users.Where(u => (u.DisplayName != "N/A" && u.DisplayName != "(Remove Assign User)")).OrderBy(u => u.FirstName), "Id", "DisplayName", model.SelectedUsers);
            return View(model);
        }

        //Post
        [HttpPost]
        [Authorize(Roles = "Admin, Project Manager, Guest")]
        public ActionResult ProjectAssign(ProjectAssignUsersViewModel model)
        {
            var project = db.Projects.Find(model.Project.Id);
            ProjectAssignHelper helper = new ProjectAssignHelper(db);

            foreach (var user in db.Users.Select(r => r.Id).ToList())
            {
                helper.RemoveProjectFromUser(project.Id, user);
            }
            if (model.SelectedUsers != null)
            {
                foreach (var user in model.SelectedUsers)
                {
                    helper.AddProjectToUser(project.Id, user);
                }
            }
            return RedirectToAction("Index", "Home");
        }


        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            var project  = db.Projects.Find(id);
            var tickets = db.Tickets.Where(t => t.Status.Name != "Closed" && t.ProjectId == id).ToList();


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            if (tickets.Count > 0)
            {
                project.Tickets = tickets;
                return View(project);
            }
            else
            {
                project.IsDeleted = true;
                //db.Projects.Remove(project);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");

            }
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            project.IsDeleted = true;
            //db.Projects.Remove(project);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
