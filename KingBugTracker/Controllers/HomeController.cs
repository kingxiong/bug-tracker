﻿using KingBugTracker.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;

namespace KingBugTracker.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View(db.Projects.ToList().Where(p => p.IsDeleted == false));
        }

        //public ActionResult Index(int? page, string searchStr)
        //{
        //    ViewBag.Search = searchStr;
        //    var projectList = IndexSearch(searchStr);

        //    int pageSize = 3; // display three blog post at a time before returning the view
        //    int pageNumber = (page ?? 1);


        //    return View(projectList.ToPagedList(pageNumber, pageSize).Where(m => m.IsDeleted == false));
        //}
        //public IQueryable<Project> IndexSearch(string searchStr)
        //{
        //    IQueryable<Project> result = null;
        //    if (searchStr != null)
        //    {
        //        result = db.Projects.AsQueryable();
        //        result = result.Where(p => p.Body.Contains(searchStr) ||
        //                                   p.Created.ToString().Contains(searchStr) ||
        //                                   p.Id.ToString().Contains(searchStr) ||
        //                                   p.Updated.ToString().Contains(searchStr) ||
        //                                   p.Tickets.Any(m => m.Body.Contains(searchStr)) ||
        //                                   p.Title.Contains(searchStr) ||
        //                                   p.Users.Any(m => m.FirstName.Contains(searchStr) ||
        //                                                    m.DisplayName.Contains(searchStr) ||
        //                                                    m.LastName.Contains(searchStr)));
        //    }
        //    else
        //    {
        //        result = db.Projects.AsQueryable();
        //    }
        //    return result.OrderByDescending(p => p.Created);
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    var body = "<p>Email From: <b>{0}</b>({1})</p><p>Message:</p><p>{2}</p>";
                    //model.Body = "This is a message from your personal site. The name and the email of the contacting person is above";
                    var email = new MailMessage(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["emailTo"])
                    {
                        Subject = "Welcome to kingCom",
                        Body = string.Format(body, model.FromName, model.FromEmail, model.Body),
                        IsBodyHtml = true
                    };

                    var svc = new PersonalEmail();
                    await svc.SendAsync(email);

                    return RedirectToAction("Sent");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await Task.FromResult(0);
                }

            }

            return View(model);
        }
        public ActionResult Sent()
        {
            return View();
        }
        

    }
}